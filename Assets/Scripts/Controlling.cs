using UnityEngine;
using Sirenix.OdinInspector;

public class Controlling : MonoBehaviour
{
    // Options
	[TitleGroup("Values")]
    public KeyCode cancel = KeyCode.Escape;
	[TitleGroup("Values")]
    public KeyCode debug = KeyCode.F3;
	[TitleGroup("Values")]
    public KeyCode switchCam = KeyCode.F5;
	[TitleGroup("Values")]
    public KeyCode switchPlayermode = KeyCode.F8;
	[TitleGroup("Values")]
    public KeyCode switchGamemode = KeyCode.F9;
	[TitleGroup("Values")]
    public KeyCode inventory = KeyCode.E;

    // Movement
	[TitleGroup("Values")]
    public KeyCode jump = KeyCode.Space;
	[TitleGroup("Values")]
    public KeyCode run = KeyCode.LeftControl;

    // Construction/Interaction
	[TitleGroup("Values")]
    public KeyCode breakBlock = KeyCode.Mouse0;
	[TitleGroup("Values")]
    public KeyCode placeBlock = KeyCode.Mouse1;
	[TitleGroup("Values")]
    public KeyCode openMachine = KeyCode.Mouse1;

}
