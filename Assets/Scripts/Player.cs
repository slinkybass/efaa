using UnityEngine;
using Sirenix.OdinInspector;
using Cinemachine;

public class Player : MonoBehaviour
{
	private Game game;

	private CharacterController characterController;
	private Animator animator;
    private Transform lookingPointTrans;

	[TitleGroup("Values"), ValueDropdown("GetGamemodeValues"), HideInEditorMode]
	public int gamemode = 1;
	[TitleGroup("Values"), ValueDropdown("GetPlayermodeValues"), HideInEditorMode]
	public int playermode = 0;

	[TitleGroup("Properties"), HideInEditorMode, DisableInPlayMode]
    public Item selectedItem;

	// Looking
    private RaycastHit hit;
    private GameObject hitted;
	[TitleGroup("Values"), Range(0, 50)]
    public float rayLimit = 15f;
	[TitleGroup("Properties"), HideInEditorMode, DisableInPlayMode]
    public Block lookingBlock;
	[TitleGroup("Properties"), HideInEditorMode, DisableInPlayMode]
    public Vector3 lookingNewPos;
	[TitleGroup("Properties"), HideInEditorMode, DisableInPlayMode]
    public int lookingCoord;

	// Moving speed
	[TitleGroup("Values"), Range(0, 30)]
	public float walkingSpeed = 5f;
	[TitleGroup("Values"), Range(0, 30)]
	public float runningSpeed = 10f;
	[TitleGroup("Properties"), HideInEditorMode, DisableInPlayMode]
	public float actualSpeed;
	private float turnSmoothTime = 0.1f;
	private float turnSmoothVel;
	
	// Jumping
	[TitleGroup("Values"), Range(0, 25)]
	public float jumpHeight = 1f;
	private float groundedTimer;
	private float verticalVelocity;

	[TitleGroup("Properties"), HideInEditorMode, DisableInPlayMode]
	public bool isGrounded;
	[TitleGroup("Properties"), HideInEditorMode, DisableInPlayMode]
	public bool inInventory;
	[TitleGroup("Properties"), HideInEditorMode, DisableInPlayMode]
	public bool inMachine;

	private bool fpsCam = true;

	void Awake()
	{
        this.game = GameObject.Find("Game").GetComponent<Game>();

		this.characterController = GetComponent<CharacterController>();
		this.animator = GetComponent<Animator>();
        this.lookingPointTrans = transform.Find("LookingPoint").transform;
	}

	void Start ()
	{
		this.SetFirstSelectedItem();
	}

	void Update ()
	{
		this.CursorState();
		this.CrosshairState();
		this.Controls();
	}

	// Set first item to selected item
	private void SetFirstSelectedItem() {
		var itemsEnum = this.game.items.GetEnumerator();
		itemsEnum.MoveNext();
		this.selectedItem = itemsEnum.Current.Value;
	}

	// Checks and sets cursor visibility and state
	private void CursorState() {
		Cursor.visible = this.ShowCursor();
		Cursor.lockState = this.ShowCursor() ? CursorLockMode.None : CursorLockMode.Confined;
	}

	// Checks and sets crosshair visibility
	private void CrosshairState() {
		this.game.crosshairGo.SetActive(this.fpsCam && !this.ShowCursor());
	}

	private void Controls() {
		this.PressCancel();
		if (!this.game.isPaused) {
			this.PressInventory();
			this.Move();
			if (!this.ShowCursor()) {
				this.PressDebug();
				this.PressSwitchCam();
				this.PressSwitchPlayermode();
				this.PressSwitchGamemode();

				this.HitBlock();
				this.LookingCoord();

				if (this.getPlayermode() == "construction") {
					this.PressConstructionKeys();
				} else if (this.getPlayermode() == "interaction") {
					this.PressInteractionKeys();
				}
			}
		}
	}

	// Press cancel key
	private void PressCancel() {
		if (Input.GetKeyDown(this.game.controlling.cancel)) {
			if (this.inInventory || this.inMachine) {
				if (this.inInventory) {
					this.inInventory = false;
					this.game.inventory.Active(false);
				}
				if (this.inMachine) {
					this.inMachine = false;
					this.game.machine.Active(false);
				}
			} else {
				this.game.isPaused = !this.game.isPaused;
				this.game.menu.Active(this.game.isPaused);
			}
		}
	}

	// Press cancel key
	private void PressInventory() {
		if (Input.GetKeyDown(this.game.controlling.inventory)) {
			this.inInventory = !this.inInventory;
			this.game.inventory.Active(this.inInventory);
		}
	}

	// Move the player
	private void Move()
	{
		// Thanks to: Brackeys (https://www.youtube.com/watch?v=4HpC--2iowE)

		// Walk / Run
		float h = Input.GetAxisRaw("Horizontal");
		float v = Input.GetAxisRaw("Vertical");
		Vector3 direction = new Vector3(h, 0, v).normalized;
		Vector3 moveDirection = Vector3.zero;
		this.actualSpeed = 0;

		if (!this.ShowCursor() && direction.magnitude >= 0.1f) {
			// Calculate speed
			if (Input.GetKey(this.game.controlling.run)) {
				this.animator.SetInteger("Walk", 0);
				this.animator.SetInteger("Run", 1);
				this.actualSpeed = this.runningSpeed;
			} else {
				this.animator.SetInteger("Walk", 1);
				this.animator.SetInteger("Run", 0);
				this.actualSpeed = this.walkingSpeed;
			}
			// Turn
			float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + this.game.cameraGo.transform.eulerAngles.y;
			float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref this.turnSmoothVel, this.turnSmoothTime);
			transform.rotation = Quaternion.Euler(0, angle, 0);
			// Move direction
			moveDirection = Quaternion.Euler(0, targetAngle, 0) * Vector3.forward;
			moveDirection = moveDirection.normalized * this.actualSpeed;
		} else {
			this.animator.SetInteger("Walk", 0);
			this.animator.SetInteger("Run", 0);
		}

		// Jump / Gravity
        if (this.characterController.isGrounded) {
            this.groundedTimer = 0.2f;
        }
        if (this.groundedTimer > 0) {
            this.groundedTimer -= Time.deltaTime;
        } else {
			this.animator.SetInteger("Falling", 1);
			this.isGrounded = false;
		}
        if (this.characterController.isGrounded && this.verticalVelocity < 0) {
			this.animator.SetInteger("Jump", 0);
			this.animator.SetInteger("Falling", 0);
            this.verticalVelocity = 0f;
			this.isGrounded = true;
        }
        this.verticalVelocity -= -Physics.gravity.y * Time.deltaTime;
        if (!this.ShowCursor() && Input.GetKeyDown(this.game.controlling.jump)) {
            if (this.groundedTimer > 0) {
				this.animator.SetInteger("Jump", 1);
                this.groundedTimer = 0;
                this.verticalVelocity += Mathf.Sqrt(this.jumpHeight * 2 * -Physics.gravity.y);
            }
        }
        moveDirection.y = this.verticalVelocity;

		// Worldsize limit
		float myX = transform.position.x;
		float myZ = transform.position.z;
		if (myX > this.game.woldSizeX-1.1f || myX < 0.1f) {
			moveDirection.x = 0;
		}
		if (myZ > this.game.woldSizeY-1.1f || myZ < 0.1f) {
			moveDirection.z = 0;
		}

		// Move
		this.characterController.Move(moveDirection * Time.deltaTime);

		// Move head
		if (!this.ShowCursor()) {
			this.lookingPointTrans.rotation = Quaternion.RotateTowards(this.lookingPointTrans.rotation, this.game.cameraGo.transform.rotation, 1500);
		}
	}

	// Press debug key
	private void PressDebug() {
		if (Input.GetKeyDown(this.game.controlling.debug)) {
			this.game.debugScreen.Alternate();
		}
	}

	// Press switchCam key
    private void PressSwitchCam()
    {
		if (Input.GetKeyDown(this.game.controlling.switchCam)) {
			Camera cam = this.game.cameraGo.GetComponent<Camera>();
			CinemachineVirtualCamera fpcam = this.game.firstPersonCameraGo.GetComponent<CinemachineVirtualCamera>();
			CinemachineFreeLook tpcam = this.game.thirdPersonCameraGo.GetComponent<CinemachineFreeLook>();

			if (fpcam.m_Priority == 1) {
				//TPC
				cam.cullingMask = ~0;
				fpcam.m_Priority = 0;
				tpcam.m_Priority = 1;
				this.fpsCam = false;
			} else {
				//FPC
				cam.cullingMask = ~LayerMask.GetMask("Player");
				fpcam.m_Priority = 1;
				tpcam.m_Priority = 0;
				this.fpsCam = true;
			}
		}
    }

	// Press switchPlayermode key
	private void PressSwitchPlayermode() {
		if (Input.GetKeyDown(this.game.controlling.switchPlayermode)) {
			int nextPlayermode = this.playermode + 1;
			nextPlayermode = nextPlayermode + 1 > this.game.playermodes.Count ? 0 : nextPlayermode;
			this.setPlayermode(nextPlayermode);
		}
	}

	// Press switchGamemode key
	private void PressSwitchGamemode() {
		if (Input.GetKeyDown(this.game.controlling.switchGamemode)) {
			int nextGamemode = this.gamemode + 1;
			nextGamemode = nextGamemode + 1 > this.game.gamemodes.Count ? 0 : nextGamemode;
			this.setGamemode(nextGamemode);
		}
	}

	// Press breakBlock/placeBlock key
    private void PressConstructionKeys()
    {
		if (Input.GetKeyDown(this.game.controlling.breakBlock)) {
			// Break lookingBlock
			if (this.lookingBlock && this.lookingBlock.transform.parent.name == "Blocks") {
				Destroy(this.lookingBlock.gameObject);
			}
		}
		if (Input.GetKeyDown(this.game.controlling.placeBlock)) {
			// Place new block on top of lookingBlock
			if (this.lookingBlock) {
				if (!this.game.transform.Find("Blocks/" + this.lookingNewPos.x + ", " + this.lookingNewPos.y + ", " + this.lookingNewPos.z)) {
					Voxel.Place(this.selectedItem, this.lookingNewPos);
				}
			}
		}
    }

	// Press openMachine key
    private void PressInteractionKeys()
    {
		if (Input.GetKeyDown(this.game.controlling.openMachine)) {
			// Open machine of lookingBlock
			if (this.lookingBlock && this.lookingBlock.item.isMachine) {
				this.inInventory = true;
				this.game.inventory.Active(true);
				this.inMachine = true;
				this.game.machine.Active(true);
			}
		}
    }

	// Set lookingBlock
    private void HitBlock()
    {
        int layerMask = LayerMask.GetMask("Block", "BlockNoCollision");
        // Raycast
		if (Physics.Raycast(this.lookingPointTrans.position, this.lookingPointTrans.forward, out this.hit, this.rayLimit, layerMask)) {
            Debug.DrawLine(this.lookingPointTrans.position, this.hit.point, Color.green);
            GameObject newHitted = this.hit.transform.gameObject;

            // Remove Outline from old hitted GameObject
            if (this.hitted != null && this.hitted != newHitted) {
                this.hitted.GetComponent<Block>().setHitted(false);
            }

            // Set looking block
            Block supposedBlock = newHitted.GetComponent<Block>();
            if (supposedBlock) {
                if (this.hitted != newHitted) {
                    this.hitted = newHitted;
                    this.hitted.GetComponent<Block>().setHitted(true);
                    this.lookingBlock = supposedBlock;
                }
                this.lookingNewPos = this.hit.transform.position + this.hit.normal;
            } else {
                if (this.hitted != null) {
                    this.hitted.GetComponent<Block>().setHitted(false);
                    this.hitted = null;
                }
                this.lookingBlock = null;
                this.lookingNewPos = Vector3.zero;
            }
        } else {
            if (this.hitted != null) {
                this.hitted.GetComponent<Block>().setHitted(false);
                this.hitted = null;
            }
            this.lookingBlock = null;
            this.lookingNewPos = Vector3.zero;
        }
    }

	// Set lookingCoord
    private void LookingCoord()
    {
		float lookingAngle = this.lookingPointTrans.rotation.eulerAngles.y;
		if (lookingAngle > 315 || lookingAngle <= 45) {
			this.lookingCoord = 1;
		} else if (lookingAngle > 45 && lookingAngle <= 135) {
			this.lookingCoord = 2;
		} else if (lookingAngle > 135 && lookingAngle <= 225) {
			this.lookingCoord = 3;
		} else if (lookingAngle > 225 && lookingAngle <= 315) {
			this.lookingCoord = 4;
		}
    }

	public bool ShowCursor() {
		return this.game.isPaused || this.inInventory || this.inMachine;
	}
	
	public void setGamemode(int gm) { this.gamemode = gm; }
	public void setGamemode(string gm) { this.gamemode = this.game.gamemodes.IndexOf(gm); }
	public string getGamemode() { return this.game.gamemodes[this.gamemode]; }
    private ValueDropdownList<int> GetGamemodeValues() {
        ValueDropdownList<int> gamemodes = new ValueDropdownList<int>();
		if (this.game != null) {
			for (int i = 0; i < this.game.gamemodes.Count; i++) {
				gamemodes.Add(this.game.gamemodes[i], i);
			}
		}
        return gamemodes;
    }
	public void setPlayermode(int pm) { this.playermode = pm; }
	public void setPlayermode(string pm) { this.playermode = this.game.playermodes.IndexOf(pm); }
	public string getPlayermode() { return this.game.playermodes[this.playermode]; }
    private ValueDropdownList<int> GetPlayermodeValues() {
        ValueDropdownList<int> playermodes = new ValueDropdownList<int>();
		if (this.game != null) {
			for (int i = 0; i < this.game.playermodes.Count; i++) {
				playermodes.Add(this.game.playermodes[i], i);
			}
        }
        return playermodes;
    }

}
