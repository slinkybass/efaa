using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class JsonReader : MonoBehaviour 
{
	public static Dictionary<string, Item> LoadItems(string mod)
	{
		Dictionary<string, Item> items = new Dictionary<string, Item>();
		Object[] jsonFileArray = Resources.LoadAll("Data/Items/" + mod, typeof(TextAsset));

		foreach (Object asset in jsonFileArray) {
			string assetPath = AssetDatabase.GetAssetPath(asset);
			assetPath = assetPath.Replace("Assets/Resources/", "");
			Item item = LoadItem(mod, assetPath);
			items.Add(item.id, item);
		}

		return items;
	}

	private static Item LoadItem(string mod, string path)
	{
		ItemJson itemJson = JsonUtility.FromJson<ItemJson>(LoadJson(path));
		string itemName = path.Substring(path.LastIndexOf("/")+1).Replace(".json", "");
		Item item = new Item(mod, itemName, itemJson);
		return item;
	}

	private static string LoadJson(string path)
	{
		string jsonFilePath = path.Replace(".json", "");
		TextAsset loadedJsonFile = Resources.Load<TextAsset>(jsonFilePath);
		return loadedJsonFile.text;
	}

}