using UnityEngine;

public class Voxel : MonoBehaviour
{
    public static void SpawnPlayer(Vector3 position)
    {
        GameObject game = GameObject.Find("Game");

        GameObject prefabPlayer = Resources.Load<GameObject>("Prefabs/Player");
        GameObject goPlayer = Instantiate(prefabPlayer) as GameObject;

        goPlayer.transform.parent = game.transform;
        goPlayer.name = "Player";

        goPlayer.transform.position = position;
    }

    public static void PlaceBase(int sizeX, int sizeY, Item item)
    {
        GameObject game = GameObject.Find("Game");
        Game gameGame = game.GetComponent<Game>();

        for (int x = 0; x < sizeX; x++) {
            for (int z = 0; z < sizeY; z++) {
                Place(item, new Vector3(x, -1, z), "Base");
            }
        }
    }

    public static GameObject Place(Item item, Vector3 position, string parentName = null)
    {
        if (item == null) {
            throw new System.Exception("The Item don't exists");
        } else if (!item.prefab) {
            throw new System.Exception("The Item has no Prefab");
        }

        GameObject parent = parentName == null ? GameObject.Find("Game/Blocks") : GameObject.Find("Game/" + parentName);

        GameObject go = Instantiate(item.prefab, parent.transform) as GameObject;
        MeshCollider mCollider = go.AddComponent<MeshCollider>();
        Material material = go.GetComponentInChildren<Renderer>().material;
        material.shader = Shader.Find("Standard");
        material.EnableKeyword("_EMISSION");

        Block goBlock = go.AddComponent<Block>();
        goBlock.item = item;
        goBlock.pos = position;

        return go;
    }

}