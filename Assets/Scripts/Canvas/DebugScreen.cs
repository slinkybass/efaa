using UnityEngine;
using UnityEngine.UI;

public class DebugScreen : MonoBehaviour
{
	private Game game;

    private GameObject canvasGo;
    private Text fpsTxt;
    private Text timeTxt;
    private Text playerModeGameModeTxt;
    private Text playerCoordsTxt;
    private Text playerLookingCoordTxt;
    private Text playerLookingAtTxt;
    private Text playerGroundedTxt;
    private Text playerSpeedTxt;

    private int fps_frameCounter = 0;
    private float fps_timeCounter = 0.0f;
    private float fps_lastFramerate = 0.0f;
    private float fps_refreshTime = 1.0f;

    void Awake()
    {
        this.game = GameObject.Find("Game").GetComponent<Game>();

        this.canvasGo = transform.Find("Canvas").gameObject;
        this.fpsTxt = this.canvasGo.transform.Find("FPS").GetComponent<Text>();
        this.timeTxt = this.canvasGo.transform.Find("Time").GetComponent<Text>();
        this.playerModeGameModeTxt = this.canvasGo.transform.Find("PlayerModeGameMode").GetComponent<Text>();
        this.playerCoordsTxt = this.canvasGo.transform.Find("PlayerCoords").GetComponent<Text>();
        this.playerLookingCoordTxt = this.canvasGo.transform.Find("PlayerLookingCoord").GetComponent<Text>();
        this.playerLookingAtTxt = this.canvasGo.transform.Find("PlayerLookingAt").GetComponent<Text>();
        this.playerGroundedTxt = this.canvasGo.transform.Find("PlayerGrounded").GetComponent<Text>();
        this.playerSpeedTxt = this.canvasGo.transform.Find("PlayerSpeed").GetComponent<Text>();
    }

    void Update()
    {
        this.checkFPS();
        this.setValues();
    }

    // Checks the FPS in game
    private void checkFPS() {
        if (this.fps_timeCounter < this.fps_refreshTime) {
            this.fps_timeCounter += Time.deltaTime;
            this.fps_frameCounter++;
        } else {
            this.fps_lastFramerate = this.fps_frameCounter/this.fps_timeCounter;
            this.fps_frameCounter = 0;
            this.fps_timeCounter = 0.0f;
        }
    }

    // Set values in screen
    private void setValues() {
        // FPS
        float fpsVal = Mathf.Floor(this.fps_lastFramerate);
        this.fpsTxt.text = "FPS: " + fpsVal;
        
        // Time
        this.timeTxt.text = "Time: " + this.game.sun.hour + ":" + Mathf.Floor(this.game.sun.minute).ToString("00") + " (Day " + this.game.sun.day + ", Month " + this.game.sun.month + ", Year " + this.game.sun.year + ")";

        if (this.game.player != null) {
            // Playermode and Gamemode
            this.playerModeGameModeTxt.text = "Playermode: " + this.game.player.getPlayermode() + ", Gamemode: " + this.game.player.getGamemode();

            // Player coords
            float x = this.game.player.transform.position.x;
            float y = this.game.player.transform.position.y;
            float z = this.game.player.transform.position.z;
            this.playerCoordsTxt.text = "Position: x: " + x.ToString("F1") + ", y: " + y.ToString("F1") + ", z: " + z.ToString("F1");

            // Looking coord
            string lookingCoord = "-";
            if (this.game.player.lookingCoord == 1) {
                lookingCoord = "North";
            } else if (this.game.player.lookingCoord == 2) {
                lookingCoord = "East";
            } else if (this.game.player.lookingCoord == 3) {
                lookingCoord = "South";
            } else if (this.game.player.lookingCoord == 4) {
                lookingCoord = "West";
            }
            this.playerLookingCoordTxt.text = "Looking coord: " + lookingCoord;

            // Looking at
            string lookingAt = "-";
            if (this.game.player.lookingBlock) {
                Block lookingBlock = this.game.player.lookingBlock;
                lookingAt = lookingBlock.item.id + " (" + lookingBlock.pos.x + ", " + lookingBlock.pos.y + ", " + lookingBlock.pos.z + ")";
            }
            this.playerLookingAtTxt.text = "Looking at: " + lookingAt;

            // Player grounded
            this.playerGroundedTxt.text = "Grounded: " + this.game.player.isGrounded;

            // Player speed
            this.playerSpeedTxt.text = "Speed: " + this.game.player.actualSpeed;
        }
    }

    public void Alternate() {
        this.canvasGo.SetActive(!this.canvasGo.activeSelf);
    }

}
