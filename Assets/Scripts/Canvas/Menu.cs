using UnityEngine;

public class Menu : MonoBehaviour
{
    private Game game;

    private GameObject canvasGo;

    void Awake()
    {
        this.game = GameObject.Find("Game").GetComponent<Game>();

        this.canvasGo = gameObject.transform.Find("Canvas").gameObject;
    }

    // Click on resume button
    private void Resume() {
		this.game.isPaused = !this.game.isPaused;
        this.Active(false);
    }

    // Click on exit button
    private void Exit() {
		Application.Quit();
    }

    public void Active(bool active) {
        this.canvasGo.SetActive(active);
    }

}
