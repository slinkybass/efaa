using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class PlayerMode : MonoBehaviour
{
    private Game game;

    private GameObject canvasGo;
    private GameObject playermodeGo;
    private GameObject playermodeIconGo;
    private GameObject selectedItemGo;
    private GameObject selectedItemIconGo;

	private int playermode = -1;
	private Item selectedItem;

	private Dictionary<int, Texture2D> playermodeIcons = new Dictionary<int, Texture2D>();

    void Awake()
    {
        this.game = GameObject.Find("Game").GetComponent<Game>();

        this.canvasGo = gameObject.transform.Find("Canvas").gameObject;
        this.playermodeGo = this.canvasGo.transform.Find("Playermode").gameObject;
        this.playermodeIconGo = this.playermodeGo.transform.Find("Panel/Icon").gameObject;
        this.selectedItemGo = this.canvasGo.transform.Find("SelectedItem").gameObject;
        this.selectedItemIconGo = this.selectedItemGo.transform.Find("Panel/Icon").gameObject;
    }

    void Start()
    {
        this.loadTextures();
    }

    void Update()
    {
        this.setValues();
	}

    // Click on exit button
    private void loadTextures() {
        foreach (string pm in this.game.playermodes) {
            int pmKey = this.game.playermodes.IndexOf(pm);
            playermodeIcons.Add(pmKey, Resources.Load<Texture2D>("Textures/playermode_" + pmKey));
        }
    }

    // Set values in screen
    private void setValues() {
        if (this.game.player != null) {
            if (this.playermode != this.game.player.playermode) {
                this.playermode = this.game.player.playermode;
                this.playermodeIconGo.GetComponent<RawImage>().texture = playermodeIcons[this.playermode];
                this.selectedItemGo.SetActive(this.playermode == 0);
            }
            if (this.selectedItem != this.game.player.selectedItem) {
                this.selectedItem = this.game.player.selectedItem;
                this.selectedItemIconGo.GetComponent<RawImage>().texture = this.selectedItem.icon;
            }
        }
    }

}
