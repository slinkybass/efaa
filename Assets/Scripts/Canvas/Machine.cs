using UnityEngine;
using Lean.Transition;

public class Machine : MonoBehaviour
{
    private Game game;

    private GameObject canvasGo;
    private GameObject gridGo;
    private LeanManualAnimation showAnim;
    private LeanManualAnimation hideAnim;

    void Awake()
    {
        this.game = GameObject.Find("Game").GetComponent<Game>();

        this.canvasGo = gameObject.transform.Find("Canvas").gameObject;
        this.gridGo = gameObject.transform.Find("Canvas/Machine/Panel/Grid").gameObject;
        this.showAnim = gameObject.transform.Find("Show").GetComponent<LeanManualAnimation>();
        this.hideAnim = gameObject.transform.Find("Hide").GetComponent<LeanManualAnimation>();
    }

    public void Active(bool active) {
        if (active) {
            this.canvasGo.SetActive(true);
            this.showAnim.BeginTransitions();
        } else {
            this.hideAnim.BeginTransitions();
            this.canvasGo.SetActive(false);
        }
    }

}
