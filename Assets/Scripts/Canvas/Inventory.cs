using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Lean.Transition;

public class Inventory : MonoBehaviour
{
    private Game game;

    private GameObject canvasGo;
    private GameObject gridGo;
    private LeanManualAnimation showAnim;
    private LeanManualAnimation hideAnim;

    private bool panelPopulated = false;

    void Awake()
    {
        this.game = GameObject.Find("Game").GetComponent<Game>();

        this.canvasGo = gameObject.transform.Find("Canvas").gameObject;
        this.gridGo = gameObject.transform.Find("Canvas/Inventory/Panel/Grid").gameObject;
        this.showAnim = gameObject.transform.Find("Show").GetComponent<LeanManualAnimation>();
        this.hideAnim = gameObject.transform.Find("Hide").GetComponent<LeanManualAnimation>();
    }

    void Update()
    {
        this.Populate();
	}

    // Populate inventory grid with items
    private void Populate() {
        if (!this.panelPopulated && this.game.items.Count > 0) {
            GameObject itemPrefab = Resources.Load<GameObject>("Prefabs/Item");
            foreach (KeyValuePair<string, Item> item in this.game.items) {
                GameObject itemGo = Instantiate(itemPrefab) as GameObject;
                itemGo.transform.SetParent(this.gridGo.transform);
                itemGo.name = item.Value.displayName;
                itemGo.transform.Find("Item").GetComponent<Button>().onClick.AddListener(delegate{this.ClickOnItem(item.Value);});
                itemGo.transform.Find("Item/Panel/Icon").GetComponent<RawImage>().texture = item.Value.icon;
                itemGo.transform.Find("Item/Hovered Info/ItemName").GetComponent<Text>().text = item.Value.displayName;
                itemGo.transform.Find("Item/Hovered Info/ItemId").GetComponent<Text>().text = item.Value.id;
            }
            this.panelPopulated = true;
        }
    }

    // Click on inventory item
    private void ClickOnItem(Item selectedItem) {
        // Sets player selectedItem
        if (this.game.player.getPlayermode() == "construction") {
            this.game.player.selectedItem = selectedItem;
        } else if (this.game.player.getPlayermode() == "interaction") {
            if (this.game.player.inMachine) {
                Debug.Log("Drag item");
            }
        }
    }

    public void Active(bool active) {
        if (active) {
            this.showAnim.BeginTransitions();
        } else {
            this.hideAnim.BeginTransitions();
        }
    }

}
