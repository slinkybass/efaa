using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class Tester : SerializedMonoBehaviour
{
	private Game game;

	void Awake()
	{
        this.game = GameObject.Find("Game").GetComponent<Game>();
	}

    [Button("Place Item", ButtonStyle.Box, Expanded = true), HideInEditorMode]
	public void PlaceItem([ValueDropdown("GetItemsValues")] string itemID, Vector3 position) {
		Voxel.Place(this.game.GetItem(itemID), position);
	}
    
    private ValueDropdownList<string> GetItemsValues() {
        ValueDropdownList<string> items = new ValueDropdownList<string>();
        foreach (KeyValuePair<string, Item> item in this.game.items) {
            items.Add(item.Value.displayName, item.Key);
        }
        return items;
    }

}
