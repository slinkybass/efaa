using UnityEngine;
using Cinemachine;

public class FirstPersonCamera : MonoBehaviour
{
	private Game game;

    private CinemachineVirtualCamera cineMachine;

    private void Awake()
    {
        this.game = GameObject.Find("Game").GetComponent<Game>();

        this.cineMachine = GetComponent<CinemachineVirtualCamera>();
    }

    private void Update()
    {
        this.FollowPlayer();
    }

    // Follow player
    private void FollowPlayer() {
        if (this.game.player != null) {
            // Look at
            if (this.cineMachine.Follow == null) {
                this.cineMachine.Follow = this.game.player.gameObject.transform.Find("LookingPoint");
            }

            // Enable/Disable movement
            if (!this.game.player.ShowCursor()) {
                this.cineMachine.GetCinemachineComponent<CinemachinePOV>().m_VerticalAxis.m_InputAxisName = "Mouse Y";
                this.cineMachine.GetCinemachineComponent<CinemachinePOV>().m_HorizontalAxis.m_InputAxisName = "Mouse X";
            } else {
                this.cineMachine.GetCinemachineComponent<CinemachinePOV>().m_VerticalAxis.m_InputAxisName = "";
                this.cineMachine.GetCinemachineComponent<CinemachinePOV>().m_HorizontalAxis.m_InputAxisName = "";
                this.cineMachine.GetCinemachineComponent<CinemachinePOV>().m_VerticalAxis.m_InputAxisValue = 0;
                this.cineMachine.GetCinemachineComponent<CinemachinePOV>().m_HorizontalAxis.m_InputAxisValue = 0;
            }
        }
    }

}
