using UnityEngine;
using Cinemachine;

public class ThirdPersonCamera : MonoBehaviour
{
	private Game game;

    private CinemachineFreeLook cineMachine;

    private void Awake()
    {
        this.game = GameObject.Find("Game").GetComponent<Game>();

        this.cineMachine = GetComponent<CinemachineFreeLook>();
    }

    private void Update()
    {
        this.FollowPlayer();
    }

    // Follow player
    private void FollowPlayer() {
        if (this.game.player != null) {
            // Look at
            if (this.cineMachine.LookAt == null) {
                Transform lookAt = this.game.player.gameObject.transform.Find("Armature");
                this.cineMachine.LookAt = lookAt;
                this.cineMachine.Follow = lookAt;
            }

            // Enable/Disable movement
            if (!this.game.player.ShowCursor()) {
                this.cineMachine.m_YAxis.m_InputAxisName = "Mouse Y";
                this.cineMachine.m_XAxis.m_InputAxisName = "Mouse X";
            } else {
                this.cineMachine.m_YAxis.m_InputAxisName = "";
                this.cineMachine.m_XAxis.m_InputAxisName = "";
                this.cineMachine.m_YAxis.m_InputAxisValue = 0;
                this.cineMachine.m_XAxis.m_InputAxisValue = 0;
            }
        }
    }

}
