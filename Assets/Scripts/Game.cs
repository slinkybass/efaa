using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class Game : SerializedMonoBehaviour
{
	[TitleGroup("Properties"), HideInPlayMode, DisableInEditorMode]
    public Player player;
	[TitleGroup("Properties"), HideInPlayMode, DisableInEditorMode]
	public Controlling controlling;
	[TitleGroup("Properties"), HideInPlayMode, DisableInEditorMode]
	public GameObject crosshairGo;
	[TitleGroup("Properties"), HideInPlayMode, DisableInEditorMode]
	public DebugScreen debugScreen;
	[TitleGroup("Properties"), HideInPlayMode, DisableInEditorMode]
	public Menu menu;
	[TitleGroup("Properties"), HideInPlayMode, DisableInEditorMode]
	public Inventory inventory;
	[TitleGroup("Properties"), HideInPlayMode, DisableInEditorMode]
	public Machine machine;
	[TitleGroup("Properties"), HideInPlayMode, DisableInEditorMode]
	public GameObject cameraGo;
	[TitleGroup("Properties"), HideInPlayMode, DisableInEditorMode]
	public GameObject firstPersonCameraGo;
	[TitleGroup("Properties"), HideInPlayMode, DisableInEditorMode]
	public GameObject thirdPersonCameraGo;
	[TitleGroup("Properties"), HideInPlayMode, DisableInEditorMode]
    public Sun sun;

	[TitleGroup("Properties"), HideInPlayMode, DisableInEditorMode]
	public List<string> gamemodes = new List<string> {"creative", "survival"};
	[TitleGroup("Properties"), HideInPlayMode, DisableInEditorMode]
	public List<string> playermodes = new List<string> {"construction", "interaction"};

	[TitleGroup("Properties"), HideInPlayMode, Range(16, 80)]
	public int woldSizeX = 32;
	[TitleGroup("Properties"), HideInPlayMode, Range(16, 80)]
	public int woldSizeY = 32;

	[TitleGroup("Properties"), HideInEditorMode, DisableInPlayMode]
	public bool isPaused;

	[TitleGroup("Values"), HideInEditorMode, DictionaryDrawerSettings(DisplayMode = DictionaryDisplayOptions.Foldout)]
	public Dictionary<string, Item> items = new Dictionary<string, Item>();

	void Awake()
	{
		this.controlling = GetComponent<Controlling>();
        this.crosshairGo = GameObject.Find("Canvas/Crosshair");
        this.debugScreen = GameObject.Find("Canvas/DebugScreen").GetComponent<DebugScreen>();
        this.menu = GameObject.Find("Canvas/Menu").GetComponent<Menu>();
        this.inventory = GameObject.Find("Canvas/Inventory").GetComponent<Inventory>();
        this.machine = GameObject.Find("Canvas/Machine").GetComponent<Machine>();
        this.cameraGo = GameObject.Find("Camera");
		this.firstPersonCameraGo = GameObject.Find("FirstPersonCamera");
		this.thirdPersonCameraGo = GameObject.Find("ThirdPersonCamera");
        this.sun = GameObject.Find("Sun").GetComponent<Sun>();

		LoadResources("efaa");
	}

	void Start()
	{
		Voxel.PlaceBase(this.woldSizeX, this.woldSizeY, this.GetItem("efaa:grass_block"));
		Voxel.SpawnPlayer(new Vector3(this.woldSizeX/2, 0, this.woldSizeY/2));
	}

    void Update()
    {
        // Find and get Player
        if (this.player == null) {
            GameObject playerGo = GameObject.Find("Game/Player");
            if (playerGo != null) {
                this.player = playerGo.GetComponent<Player>();
            }
        }
	}

	private void LoadResources(string mod)
	{
		this.items = JsonReader.LoadItems(mod);
	}

	public Item GetItem(string itemID)
	{
		if (this.items.ContainsKey(itemID)) {
			return this.items[itemID];
		}
		return null;
	}

}
