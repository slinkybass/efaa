using UnityEngine;
using Sirenix.OdinInspector;

public class Sun : MonoBehaviour
{
	private Game game;
    
    // Clock
	[TitleGroup("Values"), HideInEditorMode, Range(0, 60)]
    public float minute = 0;
	[TitleGroup("Values"), HideInEditorMode, Range(0, 24)]
    public int hour = 12;
	[TitleGroup("Values"), HideInEditorMode, Range(0, 31)]
    public int day = 1;
	[TitleGroup("Values"), HideInEditorMode, Range(0, 13)]
    public int month = 1;
	[TitleGroup("Values"), HideInEditorMode]
    public int year = 1;
 
	[TitleGroup("Values"), Range(1, 1440)]
    public int durationDayInMins = 20;

	void Awake ()
	{
        this.game = GameObject.Find("Game").GetComponent<Game>();
	}

    void Update()
    {
        this.ClockUpdate();
        this.SunUpdate();
    }
 
    // Updates the clock based in durationDayInMins
    private void ClockUpdate()
    {
		if (!this.game.isPaused) {
            float duration = (1440 / this.durationDayInMins) / 60;
            this.minute += Time.deltaTime * duration;

            if (this.minute >= 60) {
                this.hour += 1;
                this.minute = 0;
            }
            if (this.hour >= 24) {
                this.day += 1;
                this.hour = 0;
            }
            if (this.day >= 31) {
                this.month += 1;
                this.day = 1;
            }
            if (this.month >= 13) {
                this.year += 1;
                this.month = 1;
            }
        }
    }
    
    // Move the sun around the world based in clock
    private void SunUpdate()
    {
		if (!this.game.isPaused) {
            var hourAndMinute = this.hour + (((this.minute * 100) / 60) / 100);
            transform.eulerAngles = new Vector3(((hourAndMinute * 360f) / 24f) - 90f, 270, 0);
        }
    }

}
