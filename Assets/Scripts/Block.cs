using UnityEngine;
using Sirenix.OdinInspector;

public class Block : MonoBehaviour
{
	[TitleGroup("Values")]
	public Vector3 pos;

	[TitleGroup("Properties"), DisableInPlayMode]
	public Item item;
    
    void Start()
    {
        // Set MeshCollider
        gameObject.GetComponent<MeshCollider>().sharedMesh = transform.Find("default").GetComponent<MeshFilter>().sharedMesh;

        // Set the properties of the block
        this.Size();
        this.Ubicate();
        this.Collider();
        this.Reflections();
    }
    
    void Update()
    {
        this.Reubicate();
    }
    
    // Set the size of the block
    private void Size() {
        // Bug with two gameobjects in the same pixel
        float size = this.item.size/100f - 0.0075f; 
        if (size != gameObject.transform.localScale.x) {
            gameObject.transform.localScale = new Vector3(size, size, size);
        }
    }
    
    // Set the position of the block
    private void Ubicate() {
        gameObject.name = this.pos.x + ", " + this.pos.y + ", " + this.pos.z;
        transform.position = this.pos;
    }
    
    // Check and set the position of the block
    private void Reubicate() {
        if (this.pos != transform.position) {
            gameObject.name = this.pos.x + ", " + this.pos.y + ", " + this.pos.z;
            transform.position = this.pos;
        }
    }
    
    // Set the collider of the block
    private void Collider() {
        gameObject.layer = item.hasCollider ? LayerMask.NameToLayer("Block") : LayerMask.NameToLayer("BlockNoCollision");
    }
    
    // Set the reflections of the block
    private void Reflections() {
        Material material = GetComponentInChildren<Renderer>().sharedMaterial;
        if (item.hasReflections) {
            material.DisableKeyword("_SPECULARHIGHLIGHTS_OFF");
            material.SetFloat("_SpecularHighlights", 1f);
            material.DisableKeyword("_GLOSSYREFLECTIONS_OFF");
        } else {
            material.EnableKeyword("_SPECULARHIGHLIGHTS_OFF");
            material.SetFloat("_SpecularHighlights", 0f);
            material.EnableKeyword("_GLOSSYREFLECTIONS_OFF");
        }
    }

    // Set the block as hitted block
    public void setHitted(bool hitted = true) {
        Material material = GetComponentInChildren<Renderer>().sharedMaterial;
        float intensity = hitted ? 0.1f : 0;
        material.SetColor("_EmissionColor", Color.white * intensity);
    }

}
