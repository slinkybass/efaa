using System.Collections.Generic;

[System.Serializable]
public class ItemJson
{
    public string displayName;

    public string prefab;
    public int size = 32;
    public bool hasCollider = true;
    public bool hasReflections = false;
    public List<DropJson> drops = new List<DropJson>();
    
    public bool isMachine = false;

}