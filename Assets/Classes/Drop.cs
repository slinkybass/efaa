using Sirenix.OdinInspector;

[System.Serializable]
public class Drop
{
	[TitleGroup("Values"), Required]
    public string item;
	[TitleGroup("Values"), MinValue(1)]
    public int quantity;


    public Drop(DropJson dropJson)
    {
        this.item = dropJson.item;
        this.quantity = dropJson.quantity;
    }

}