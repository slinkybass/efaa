using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[System.Serializable]
public class Item
{
	[TitleGroup("Values"), Required]
    public string id;

	[TitleGroup("Values"), Required]
    public string displayName;

	[TitleGroup("Values"), PreviewField]
    public Texture2D icon;
	[TitleGroup("Values"), PreviewField]
    public GameObject prefab;
	[TitleGroup("Values"), Range(1, 32), Required]
    public int size;
	[TitleGroup("Values")]
    public bool hasCollider;
	[TitleGroup("Values")]
    public bool hasReflections;
	[TitleGroup("Values")]
    public List<Drop> drops = new List<Drop>();
    
	[TitleGroup("Values")]
    public bool isMachine;


    public Item(string itemMod, string itemName, ItemJson itemJson)
    {
        this.id = itemMod + ":" + itemName;
        
        this.displayName = itemJson.displayName;
        this.icon = Resources.Load<Texture2D>("Data/Textures/" + itemMod + "/" + itemName);

        if (itemJson.prefab != null) {
            this.prefab = Resources.Load<GameObject>("Data/Prefabs/" + itemMod + "/" + itemJson.prefab);
            this.size = itemJson.size;
            this.hasCollider = itemJson.hasCollider;
            this.hasReflections = itemJson.hasReflections;
            foreach (DropJson dropsJson in itemJson.drops) {
                Drop drop = new Drop(dropsJson);
                this.drops.Add(drop);
            }
            this.isMachine = itemJson.isMachine;
        }
    }

}